#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "SwinGame.h"

#define ROWS 12
#define COLUMNS 16
#define RADIUS 20
#define FRAME 4

typedef struct piece {
	bool filled;
	color clr;
	point2d pos;
} piece;

void draw_pieces(piece pieces[ROWS][COLUMNS]);
void draw_frame(void);
void draw_piece(piece temp);
void reset_current_piece(piece *current, int player);
void process_controls(piece *current, int *player, piece pieces[ROWS][COLUMNS]);
int get_column(piece temp);
bool place_piece(piece *current, piece pieces[ROWS][COLUMNS], int *player);
void reset_pieces(piece pieces[ROWS][COLUMNS]);
bool check_win(piece pieces[ROWS][COLUMNS], int row, int col);
void play_again(int *player, piece pieces[ROWS][COLUMNS]);

int main(void)
{
	open_graphics_window("Connect 4", 800, 600);

	piece pieces[ROWS][COLUMNS];
	int player = 0;
	piece current;

	reset_current_piece(&current, player);
	reset_pieces(pieces);

	do
	{
		process_events();
		clear_screen(ColorWhite);

		process_controls(&current, &player, pieces);

		draw_piece(current);
		draw_pieces(pieces);
		draw_frame();

		refresh_screen(60);
	} while(!window_close_requested());

}

bool check_win(piece pieces[ROWS][COLUMNS], int row, int col)
{
	int count, i, j;

	// Horizontal
	for (i = col - 3, count = 1; i <= col + 3 && i < COLUMNS; i++)
	{
		if(i >= 0)
		{
			if(pieces[row][col].clr == pieces[row][i].clr && pieces[row][i].filled && col != i)
			{
				count++;
			}
			else if(count < 4 && col != i)
			{
				count = 1;
			}
		}
		else 
		{
			i = -1;
		}

		if(count >= 4)
		{
			return true;
		}
	}

	// Vertical
	for (i = row - 3, count = 1; i <= row + 3 && i < ROWS; i++)
	{
		if(i >= 0)
		{
			if(pieces[row][col].clr == pieces[i][col].clr && pieces[i][col].filled && row != i)
			{
				count++;
			}
			else if(count < 4 && row != i)
			{
				count = 1;
			}
		}
		else 
		{
			i = -1;
		}

		if(count >= 4)
		{
			return true;
		}
	}

	// Diagonal down left to right
	for (i = col - 3, j = row - 3, count = 1;
		i <= col + 3 && i < COLUMNS && j <= row + 3 && j < ROWS; i++, j++)
	{
		if(i >= 0 && j >= 0)
		{
			if(pieces[row][col].clr == pieces[j][i].clr && pieces[j][i].filled && col != i && row != j)
			{
				count++;
			}
			else if(count < 4 && col != i && row != j)
			{
				count = 1;
			}
		}

		if(count >= 4)
		{
			return true;
		}
	}

	// Diagonal up left to right
	for (i = col - 3, j = row + 3, count = 1; i <= col + 3 && i < COLUMNS && j >= row - 3 && j >= 0; i++, j--)
	{
		if(i >= 0 && j < ROWS)
		{
			if(pieces[row][col].clr == pieces[j][i].clr && pieces[j][i].filled && col != i && row != j)
			{
				count++;
			}
			else if(count < 4 && col != i && row != j)
			{
				count = 1;
			}
		}

		if(count >= 4)
		{
			return true;
		}
	}

	// If no win detected
	return false;
}

void draw_pieces(piece pieces[ROWS][COLUMNS])
{
	// Draws each piece if filled = true

	for (int i = 0; i < ROWS; ++i)
	{
		for (int j = 0; j < COLUMNS; ++j)
		{
			draw_piece(pieces[i][j]);
		}
	}
}

void draw_piece(piece temp)
{
	if(temp.filled)
	{
		fill_circle(temp.clr, temp.pos.x, temp.pos.y, RADIUS);
	}
}

void reset_current_piece(piece *current, int player)
{
	if(!player)
	{
		current->clr = ColorRed;
	}
	else
	{
		current->clr = ColorOrange;
	}
	current->filled = true;
	current->pos.x = (screen_width() / 2) /*- (((RADIUS * 2) * COLUMNS) / 2)*/ + RADIUS;
	current->pos.y = (screen_height() / 2) - (((RADIUS * 2) * ROWS) / 2) - RADIUS - RADIUS / 2;
}

void reset_pieces(piece pieces[ROWS][COLUMNS])
{
	for (int i = 0; i < ROWS; ++i)
	{
		for (int j = 0; j < COLUMNS; ++j)
		{
			pieces[i][j].filled = false;
			pieces[i][j].clr = ColorWhite;
			pieces[i][j].pos.x = (screen_width() / 2) - (((RADIUS * 2) * COLUMNS) / 2) + ((RADIUS * 2) * j) + RADIUS;
			pieces[i][j].pos.y = (screen_height() / 2) - (((RADIUS * 2) * ROWS) / 2) + ((RADIUS * 2) * i) + RADIUS;
		}
	}
}

void process_controls(piece *current, int *player, piece pieces[ROWS][COLUMNS])
{	
	if(key_typed(LEFT_KEY))
	{
		current->pos.x -= RADIUS * 2;
		if(current->pos.x < (screen_width() / 2) - (((RADIUS * 2) * COLUMNS) / 2) + RADIUS)
		{
			current->pos.x = (screen_width() / 2) - (((RADIUS * 2) * COLUMNS) / 2) + RADIUS;
		}
	}
	if(key_typed(RIGHT_KEY))
	{
		current->pos.x += RADIUS * 2;
		if(current->pos.x > (screen_width() / 2) + (((RADIUS * 2) * COLUMNS) / 2) - RADIUS)
		{
			current->pos.x = (screen_width() / 2) + (((RADIUS * 2) * COLUMNS) / 2) - RADIUS;
		}
	}
	if(key_typed(DOWN_KEY))
	{
		if(place_piece(current, pieces, player))
		{
			if(*player)
			{
				*player = 0;
			}
			else
			{
				*player = 1;
			}
			reset_current_piece(current, *player);
		}
	}
}

bool place_piece(piece *current, piece pieces[ROWS][COLUMNS], int *player)
{
	int column = get_column(*current);
	int row = -1;
	int y = -1;

	if(pieces[0][column].filled)
	{
		return false;
	}
	else if(!pieces[ROWS - 1][column].filled)
	{
		y = pieces[ROWS - 1][column].pos.y;
		row = ROWS - 1;
	}
	else
	{
		for(int i = 0; i < ROWS; i++)
		{
			if(pieces[i][column].filled && i > 0)
			{
				y = pieces[i - 1][column].pos.y;
				row = i - 1;
				break;
			}
		}
	}

	if(y > -1)
	{
		while(current->pos.y < y)
		{
			current->pos.y += RADIUS / 2;
			process_events();
			clear_screen(ColorWhite);

			draw_piece(*current);
			draw_pieces(pieces);
			draw_frame();

			refresh_screen(60);
		}

		pieces[row][column].filled = true;
		pieces[row][column].clr = current->clr;

		if(check_win(pieces, row, column))
		{
			play_again(player, pieces);
		}

		return true;
	}
	
	return false;	
}

int get_column(piece temp)
{
	return ((screen_width() / 2) - (((RADIUS * 2) * COLUMNS) / 2) - temp.pos.x) / (RADIUS * 2) * -1;
}

void draw_frame(void)
{
	// Draws the frame

	color frame_colour = ColorDarkOrange;

	int x = (screen_width() / 2) - (((RADIUS * 2) * COLUMNS) / 2);
	int y = (screen_height() / 2) - (((RADIUS * 2) * ROWS) / 2);

	// Vertical lines
	for(int i = 0; i < COLUMNS; i ++)
	{
		fill_rectangle(frame_colour,
			x + i * (RADIUS * 2) - (FRAME / 2),
			y,
			FRAME,
			ROWS * (RADIUS * 2)
		);
	}
	
	// Right edge
	fill_rectangle(frame_colour,
		x + ((RADIUS * 2) * COLUMNS) - (FRAME / 2),
		y,
		FRAME,
		ROWS * (RADIUS * 2)
	);

	// Horizontal lines
	for(int i = 0; i < ROWS; i ++)
	{
		fill_rectangle(frame_colour,
			x - (FRAME / 2), 
			y + i * (RADIUS * 2) - (FRAME / 2),
			COLUMNS * (RADIUS * 2) + FRAME,
			FRAME
		);
	}

	// Bottom edge
	fill_rectangle(frame_colour,
		x - (FRAME / 2),
		y + ((RADIUS * 2) * ROWS) - (FRAME / 2),
		COLUMNS * (RADIUS * 2) + FRAME,
		FRAME
	);
}

void play_again(int *player, piece pieces[ROWS][COLUMNS])
{
	fill_rectangle(ColorWhite, screen_width() / 4, screen_height() / 4, screen_width() / 2, screen_height() / 2 );
	draw_rectangle(ColorBlack, screen_width() / 4, screen_height() / 4, screen_width() / 2, screen_height() / 2 );
	
	char winner[15] = {0};
	if(*player)
	{
		strcat(winner, "Orange ");
	}
	else
	{
		strcat(winner, "Red ");
	}
	strcat(winner, "Wins!");
	draw_text(winner, ColorBlack, "arial.ttf", 15, screen_width() / 2 - 30, screen_height() / 2 - 30);
	draw_text("Would you like to play again? Y or N", ColorBlack, "arial.ttf", 15, screen_width() / 2 - 100, screen_height() / 2 + 30);
	
	refresh_screen(60);

	do
	{
		process_events();

		if(key_typed(YKEY))
		{
			reset_pieces(pieces);
			*player = 1;
			break;
		}

		if(key_typed(NKEY))
		{
			exit(0);
		}
	} while(!window_close_requested());
}