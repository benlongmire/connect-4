### How do I get set up? ###

Built using SwinGame - http://www.swingame.com/

You'll need to ensure you can build the project, Mac OS X terminal or MinGW on Windows.
Linux might work if SDL is installed, however I have had issues getting SwinGame to compile on Linux.

### To build and run ###

* ./clean
* ./build
* ./run

### Who do I talk to? ###

* ben@blongmire.com